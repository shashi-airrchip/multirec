var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var passport =  require('passport');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(passport.initialize());

const credits = require('./controllers/credits');
app.get('/', function (req, res) {
  res.send('Hello World!');
});
app.post('/mogo/getCreditSummary', credits.initiate);
app.post('/mogo/getCredits', credits.initiate);
app.post('/mogo/updateCredits', credits.update);
app.post('/updateCredits', credits.update);
app.post('/categories', credits.getCategory);
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});