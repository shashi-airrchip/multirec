const credits = require('../controllers/credits.js');

module.exports = function (app) {
    app.post('/credits', credits.initiate);
    app.post('/updateCredits', credits.update);
}

