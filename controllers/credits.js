const jsonData = require('./config/reportRetrival.json');

const getCategory = async(req, res)=> {
    const bodyData = req.body;
    const categoryCode = bodyData.map(res => {
            return res.categoryCode;
        });
    res.status(200).send(categoryCode);
}

const initiate = async (req, res) => {
    console.log(req.body);
    try {
        //     let data = [
        //         {
        //             "description": "Salary",
        //             "categoryCode": "SLRY",
        //             "transactionNumber": 10,
        //             "monthlyAvg": 1166.93,
        //             "total": 13965.18,
        //             "transactions": [
        //                 {
        //                     "transactionId": "f9a9840e-44d4-44d6-a150-1891823f2c74",
        //                     "date": "08/02/2021",
        //                     "category": "SLRY",
        //                     "description": "ST MICHAEL'S SALARY",
        //                     "debit": "",
        //                     "credit": "286.12",
        //                     "balance": "41147.04",
        //                     "cleanString": "ST MICHAEL'S SALARY",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "2141ab7e-212e-4ebd-aed6-b98fc3d1f59f",
        //                     "date": "30/01/2021",
        //                     "category": "SLRY",
        //                     "description": "TRANSFER FROM ABC NOMINEES PAY",
        //                     "debit": "",
        //                     "credit": "4,047.50",
        //                     "balance": "44081.57",
        //                     "cleanString": "ABC NOMINEES PAY",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "13619c8e-0d51-4ad0-9d50-de988ebba520",
        //                     "date": "24/01/2021",
        //                     "category": "SLRY",
        //                     "description": "ST MICHAEL'S SALARY",
        //                     "debit": "",
        //                     "credit": "241.08",
        //                     "balance": "42067.97",
        //                     "cleanString": "ST MICHAEL'S SALARY",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "1c3fcb71-125c-4815-b740-7d46fa40304b",
        //                     "date": "08/01/2021",
        //                     "category": "SLRY",
        //                     "description": "ST MICHAEL'S SALARY",
        //                     "debit": "",
        //                     "credit": "286.12",
        //                     "balance": "31435.64",
        //                     "cleanString": "ST MICHAEL'S SALARY",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "6883c639-73f5-4bf6-a69f-aee5c532ad1b",
        //                     "date": "26/12/2020",
        //                     "category": "SLRY",
        //                     "description": "TRANSFER FROM ABC NOMINEES PAY",
        //                     "debit": "",
        //                     "credit": "4,047.50",
        //                     "balance": "35991.02",
        //                     "cleanString": "ABC NOMINEES PAY",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "7d243258-010f-4c53-ac54-f4b974b74ac5",
        //                     "date": "24/12/2020",
        //                     "category": "SLRY",
        //                     "description": "ST MICHAEL'S SALARY",
        //                     "debit": "",
        //                     "credit": "241.08",
        //                     "balance": "36092.45",
        //                     "cleanString": "ST MICHAEL'S SALARY",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "e75f9d25-7639-4241-9d39-d299ffc67667",
        //                     "date": "10/12/2020",
        //                     "category": "SLRY",
        //                     "description": "ST MICHAEL'S SALARY",
        //                     "debit": "",
        //                     "credit": "241.08",
        //                     "balance": "33434.75",
        //                     "cleanString": "ST MICHAEL'S SALARY",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "c60133e6-4a82-4f3c-885d-13bf5bfb17f2",
        //                     "date": "26/11/2020",
        //                     "category": "SLRY",
        //                     "description": "TRANSFER FROM ABC NOMINEES PAY",
        //                     "debit": "",
        //                     "credit": "4,047.50",
        //                     "balance": "36693.60",
        //                     "cleanString": "ABC NOMINEES PAY",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "a42884b1-386e-4ff7-854b-731069ddeaca",
        //                     "date": "25/11/2020",
        //                     "category": "SLRY",
        //                     "description": "ST MICHAEL'S SALARY",
        //                     "debit": "",
        //                     "credit": "286.12",
        //                     "balance": "36929.72",
        //                     "cleanString": "ST MICHAEL'S SALARY",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "a37555bd-465c-45e2-a829-3ea7bc97b3d3",
        //                     "date": "12/11/2020",
        //                     "category": "SLRY",
        //                     "description": "ST MICHAEL'S SALARY",
        //                     "debit": "",
        //                     "credit": "241.08",
        //                     "balance": "34653.03",
        //                     "cleanString": "ST MICHAEL'S SALARY",
        //                     "netOffId": ""
        //                 }
        //             ]
        //         },
        //         {
        //             "description": "Government Payments - Pension",
        //             "categoryCode": "GOVP",
        //             "transactionNumber": 12,
        //             "monthlyAvg": 118.34,
        //             "total": 1416.3,
        //             "transactions": [
        //                 {
        //                     "transactionId": "25777a47-7f49-49a5-8dc4-7769b893aaf4",
        //                     "date": "02/02/2021",
        //                     "category": "GOVP",
        //                     "description": "DEPOSIT-CREDIT CTRLINK PENSION 059P1766602903762R",
        //                     "debit": "",
        //                     "credit": "135.80",
        //                     "balance": "39933.82",
        //                     "cleanString": "CTRLINK PENSION",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "860cee1e-2dc1-4da1-bfcb-63ebbc856a6d",
        //                     "date": "02/02/2021",
        //                     "category": "GOVP",
        //                     "description": "DEPOSIT-CREDIT CTRLINK PENSION 059P1766602903762R",
        //                     "debit": "",
        //                     "credit": "100.25",
        //                     "balance": "40034.07",
        //                     "cleanString": "CTRLINK PENSION",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "dbdb2a2d-0fa5-47cc-9f2a-71dc8cea396e",
        //                     "date": "12/01/2021",
        //                     "category": "GOVP",
        //                     "description": "DEPOSIT-CREDIT CTRLINK PENSION 059P1766602903762R",
        //                     "debit": "",
        //                     "credit": "135.80",
        //                     "balance": "28863.64",
        //                     "cleanString": "CTRLINK PENSION",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "a9590e70-d92d-492d-b8bf-1d83165032eb",
        //                     "date": "11/01/2021",
        //                     "category": "GOVP",
        //                     "description": "DEPOSIT-CREDIT CTRLINK PENSION 059P1766602903762R",
        //                     "debit": "",
        //                     "credit": "100.25",
        //                     "balance": "28963.89",
        //                     "cleanString": "CTRLINK PENSION",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "8029cab6-12e6-4cba-ad93-5ad99b7ed1e2",
        //                     "date": "27/12/2020",
        //                     "category": "GOVP",
        //                     "description": "DEPOSIT-CREDIT CTRLINK PENSION 059P1766602903762R",
        //                     "debit": "",
        //                     "credit": "135.80",
        //                     "balance": "31843.27",
        //                     "cleanString": "CTRLINK PENSION",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "8f196f31-3706-4214-869e-9817c62fc987",
        //                     "date": "27/12/2020",
        //                     "category": "GOVP",
        //                     "description": "DEPOSIT-CREDIT CTRLINK PENSION 059P1766602903762R",
        //                     "debit": "",
        //                     "credit": "100.25",
        //                     "balance": "31943.52",
        //                     "cleanString": "CTRLINK PENSION",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "0b12d879-b587-4f16-b94b-c983f749a3cf",
        //                     "date": "14/12/2020",
        //                     "category": "GOVP",
        //                     "description": "DEPOSIT-CREDIT CTRLINK PENSION 059P1766602903762R",
        //                     "debit": "",
        //                     "credit": "135.80",
        //                     "balance": "34146.93",
        //                     "cleanString": "CTRLINK PENSION",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "e1a49bf2-2a86-43b1-9a84-588cc9838e7c",
        //                     "date": "13/12/2020",
        //                     "category": "GOVP",
        //                     "description": "DEPOSIT-CREDIT CTRLINK PENSION 059P1766602903762R",
        //                     "debit": "",
        //                     "credit": "100.25",
        //                     "balance": "34247.18",
        //                     "cleanString": "CTRLINK PENSION",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "85075264-5d51-47d7-b4ee-cb7a667a89b1",
        //                     "date": "26/11/2020",
        //                     "category": "GOVP",
        //                     "description": "DEPOSIT-CREDIT CTRLINK PENSION 059P1766602903762R",
        //                     "debit": "",
        //                     "credit": "135.80",
        //                     "balance": "32545.85",
        //                     "cleanString": "CTRLINK PENSION",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "ff830d3e-80db-49a4-8506-0e30b21ef0b8",
        //                     "date": "26/11/2020",
        //                     "category": "GOVP",
        //                     "description": "DEPOSIT-CREDIT CTRLINK PENSION 059P1766602903762R",
        //                     "debit": "",
        //                     "credit": "100.25",
        //                     "balance": "32646.10",
        //                     "cleanString": "CTRLINK PENSION",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "48bad3a7-ac1f-4537-ae24-aa133bb188d0",
        //                     "date": "14/11/2020",
        //                     "category": "GOVP",
        //                     "description": "DEPOSIT-CREDIT CTRLINK PENSION 059P1766602903762R",
        //                     "debit": "",
        //                     "credit": "135.80",
        //                     "balance": "34491.17",
        //                     "cleanString": "CTRLINK PENSION",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "8d11ec61-431d-4323-ad6f-2c03be29495b",
        //                     "date": "13/11/2020",
        //                     "category": "GOVP",
        //                     "description": "DEPOSIT-CREDIT CTRLINK PENSION 059P1766602903762R",
        //                     "debit": "",
        //                     "credit": "100.25",
        //                     "balance": "34591.42",
        //                     "cleanString": "CTRLINK PENSION",
        //                     "netOffId": ""
        //                 }
        //             ]
        //         },
        //         {
        //             "description": "Government Payments - Family",
        //             "categoryCode": "GOVF",
        //             "transactionNumber": 6,
        //             "monthlyAvg": 73.81,
        //             "total": 881.88,
        //             "transactions": [
        //                 {
        //                     "transactionId": "2aa0a1f3-1e78-4097-abab-d35c889a32a5",
        //                     "date": "02/02/2021",
        //                     "category": "GOVF",
        //                     "description": "DIRECT CREDIT FAMILY ALLOWANCE AUS GOV FAMILIES 3215288 FB1G9978207492887T",
        //                     "debit": "",
        //                     "credit": "146.98",
        //                     "balance": "39798.02",
        //                     "cleanString": "GOV FAMILIES",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "679cf67e-a0f7-409a-9c2a-796e24e8bf4a",
        //                     "date": "15/01/2021",
        //                     "category": "GOVF",
        //                     "description": "DIRECT CREDIT FAMILY ALLOWANCE AUS GOV FAMILIES 3215288 FB1G9978207492887T",
        //                     "debit": "",
        //                     "credit": "146.98",
        //                     "balance": "29837.59",
        //                     "cleanString": "GOV FAMILIES",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "75e5beb0-48b4-499c-9af9-a06053a23dce",
        //                     "date": "01/01/2021",
        //                     "category": "GOVF",
        //                     "description": "DIRECT CREDIT FAMILY ALLOWANCE AUS GOV FAMILIES 3215288 FB1G9978207492887T",
        //                     "debit": "",
        //                     "credit": "146.98",
        //                     "balance": "32157.47",
        //                     "cleanString": "GOV FAMILIES",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "b75128dd-2c25-487c-9503-4c92af722f45",
        //                     "date": "15/12/2020",
        //                     "category": "GOVF",
        //                     "description": "DIRECT CREDIT FAMILY ALLOWANCE AUS GOV FAMILIES 3215288 FB1G9978207492887T",
        //                     "debit": "",
        //                     "credit": "146.98",
        //                     "balance": "34011.13",
        //                     "cleanString": "GOV FAMILIES",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "22732de2-06ad-419e-92fb-c0674d06605f",
        //                     "date": "27/11/2020",
        //                     "category": "GOVF",
        //                     "description": "DIRECT CREDIT FAMILY ALLOWANCE AUS GOV FAMILIES 3215288 FB1G9978207492887T",
        //                     "debit": "",
        //                     "credit": "146.98",
        //                     "balance": "33194.63",
        //                     "cleanString": "GOV FAMILIES",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "d57c2c1c-7fc2-4150-aa65-b5e887ddd15d",
        //                     "date": "16/11/2020",
        //                     "category": "GOVF",
        //                     "description": "DIRECT CREDIT FAMILY ALLOWANCE AUS GOV FAMILIES 3215288 FB1G9978207492887T",
        //                     "debit": "",
        //                     "credit": "146.98",
        //                     "balance": "35474.37",
        //                     "cleanString": "GOV FAMILIES",
        //                     "netOffId": ""
        //                 }
        //             ]
        //         },
        //         {
        //             "description": "Other Income - Reimbursements",
        //             "categoryCode": "OTHIR",
        //             "transactionNumber": 1,
        //             "monthlyAvg": 25.01,
        //             "total": 300.0,
        //             "transactions": [
        //                 {
        //                     "transactionId": "b943fbd8-3295-4d0e-bf81-c14aec9d3ad2",
        //                     "date": "16/11/2020",
        //                     "category": "OTHIR",
        //                     "description": "DEPOSIT - INTERNET ONLINE BANKING 2594426 FNDS TFR EXPENSE 21-MAR",
        //                     "debit": "",
        //                     "credit": "300.00",
        //                     "balance": "35774.37",
        //                     "cleanString": "- INTERNET ONLINE BANKING FNDS EXPENSE",
        //                     "netOffId": ""
        //                 }
        //             ]
        //         },
        //         {
        //             "description": "Child Support Credit",
        //             "categoryCode": "CHDC",
        //             "transactionNumber": 3,
        //             "monthlyAvg": 11.59,
        //             "total": 137.67,
        //             "transactions": [
        //                 {
        //                     "transactionId": "d7c937e9-d7e1-4541-8582-3a48fd0fc342",
        //                     "date": "25/01/2021",
        //                     "category": "CHDC",
        //                     "description": "DIRECT CREDIT XX9523 CHILD SUPPORT CS **2134509",
        //                     "debit": "",
        //                     "credit": "45.89",
        //                     "balance": "42605.59",
        //                     "cleanString": "CHILD SUPPORT",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "ec88cd2e-a5a4-48c3-b93b-b35cf2de2403",
        //                     "date": "18/12/2020",
        //                     "category": "CHDC",
        //                     "description": "DIRECT CREDIT XX9523 CHILD SUPPORT CS **2134509",
        //                     "debit": "",
        //                     "credit": "45.89",
        //                     "balance": "34874.92",
        //                     "cleanString": "CHILD SUPPORT",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "6c5589cd-32fe-4af3-bd32-d92350df5c92",
        //                     "date": "21/11/2020",
        //                     "category": "CHDC",
        //                     "description": "DIRECT CREDIT XX9523 CHILD SUPPORT CS **2134509",
        //                     "debit": "",
        //                     "credit": "45.89",
        //                     "balance": "35622.54",
        //                     "cleanString": "CHILD SUPPORT",
        //                     "netOffId": ""
        //                 }
        //             ]
        //         },
        //         {
        //             "description": "Loan Credit",
        //             "categoryCode": "LONC",
        //             "transactionNumber": 1,
        //             "monthlyAvg": 29.28,
        //             "total": 350.0,
        //             "transactions": [
        //                 {
        //                     "transactionId": "90df6ca7-71b5-4fab-abbb-98ca9186a3f1",
        //                     "date": "07/01/2021",
        //                     "category": "LONC",
        //                     "description": "DEPOSIT - INTERNET ONLINE BANKING 2261012 FNDS TFR MORTGAGE FUND 04-FEB",
        //                     "debit": "",
        //                     "credit": "350.00",
        //                     "balance": "31457.92",
        //                     "cleanString": "- INTERNET ONLINE BANKING FNDS MORTGAGE FUND",
        //                     "netOffId": ""
        //                 }
        //             ]
        //         },
        //         {
        //             "description": "Loan Credit - Payday Loans",
        //             "categoryCode": "LONP",
        //             "transactionNumber": 1,
        //             "monthlyAvg": 50.02,
        //             "total": 600.0,
        //             "transactions": [
        //                 {
        //                     "transactionId": "e672eeb2-9561-41f1-81e2-a3464d475640",
        //                     "date": "19/01/2021",
        //                     "category": "LONP",
        //                     "description": "FIRST STOP MONEY FSM XX5099",
        //                     "debit": "",
        //                     "credit": "600.00",
        //                     "balance": "42440.61",
        //                     "cleanString": "FIRST STOP MONEY FSM",
        //                     "netOffId": ""
        //                 }
        //             ]
        //         },
        //         {
        //             "description": "Deposits",
        //             "categoryCode": "DPST",
        //             "transactionNumber": 1,
        //             "monthlyAvg": 25.01,
        //             "total": 300.0,
        //             "transactions": [
        //                 {
        //                     "transactionId": "ef8dbb07-64ea-4d3d-b08b-c8bf45b8d589",
        //                     "date": "02/01/2021",
        //                     "category": "DPST",
        //                     "description": "DEPOSIT INTERNET DEPOSIT 05MAR14",
        //                     "debit": "",
        //                     "credit": "300.00",
        //                     "balance": "32031.64",
        //                     "cleanString": "INTERNET DEPOSIT",
        //                     "netOffId": ""
        //                 }
        //             ]
        //         },
        //         {
        //             "description": "Deposits - Adjustments / Refunds",
        //             "categoryCode": "DEPA",
        //             "transactionNumber": 1,
        //             "monthlyAvg": 3.05,
        //             "total": 36.3,
        //             "transactions": [
        //                 {
        //                     "transactionId": "045c6cac-0a12-4603-b643-7e5cb47bde13",
        //                     "date": "12/11/2020",
        //                     "category": "DEPA",
        //                     "description": "DEPOSIT MCARE BENEFITS 24137482 AYWQ",
        //                     "debit": "",
        //                     "credit": "36.30",
        //                     "balance": "34411.95",
        //                     "cleanString": "MCARE BENEFITS",
        //                     "netOffId": ""
        //                 }
        //             ]
        //         },
        //         {
        //             "description": "Reversal",
        //             "categoryCode": "RVRS",
        //             "transactionNumber": 3,
        //             "monthlyAvg": 119.26,
        //             "total": 1426.14,
        //             "transactions": [
        //                 {
        //                     "transactionId": "ebd5ceb9-a7c9-443c-af4d-f8fc4d393480",
        //                     "date": "03/01/2021",
        //                     "category": "RVRS",
        //                     "description": "REVERSAL OF PAYMENT",
        //                     "debit": "",
        //                     "credit": "323.72",
        //                     "balance": "31731.64",
        //                     "cleanString": "REVERSAL OF PAYMENT",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "1d223733-ae4c-4f61-9171-f9757905b7fb",
        //                     "date": "03/12/2020",
        //                     "category": "RVRS",
        //                     "description": "REVERSAL OF PAYMENT",
        //                     "debit": "",
        //                     "credit": "323.72",
        //                     "balance": "33296.80",
        //                     "cleanString": "REVERSAL OF PAYMENT",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "5af2d8fb-308c-4d2d-adff-3c3672cd4e0d",
        //                     "date": "19/11/2020",
        //                     "category": "RVRS",
        //                     "description": "DIRECT DEBIT DISHONOUR",
        //                     "debit": "",
        //                     "credit": "778.70",
        //                     "balance": "35572.54",
        //                     "cleanString": "DISHONOUR",
        //                     "netOffId": ""
        //                 }
        //             ]
        //         },
        //         {
        //             "description": "Other Transfer Credits",
        //             "categoryCode": "OTFC",
        //             "transactionNumber": 2,
        //             "monthlyAvg": 300.73,
        //             "total": 3600.0,
        //             "transactions": [
        //                 {
        //                     "transactionId": "818e650e-c9a2-44bb-9693-ed7366acabd2",
        //                     "date": "10/01/2021",
        //                     "category": "OTFC",
        //                     "description": "DEPOSIT - INTERNET ONLINE BANKING 2004186 FNDS TFR 25-JAN",
        //                     "debit": "",
        //                     "credit": "3,000.00",
        //                     "balance": "31963.89",
        //                     "cleanString": "- INTERNET ONLINE BANKING FNDS TFR",
        //                     "netOffId": ""
        //                 },
        //                 {
        //                     "transactionId": "9a7d9dbb-0198-45b6-9bfd-767e88fdbcf7",
        //                     "date": "13/12/2020",
        //                     "category": "OTFC",
        //                     "description": "DEPOSIT - INTERNET ONLINE BANKING 2565332 FNDS TFR ADC 23-FEB",
        //                     "debit": "",
        //                     "credit": "600.00",
        //                     "balance": "34847.18",
        //                     "cleanString": "- INTERNET ONLINE BANKING FNDS ADC",
        //                     "netOffId": ""
        //                 }
        //             ]
        //         }
        //     ];
        // const filterData = data.filter(function (res) {
        //     return res.categoryCode === req.body.categoryCode;
        // });
        // const summary = {
        //     "success": true,
        //     "message": "Data fetched",
        //     "description": filterData[0].description,
        //     "categoryCode": filterData[0].categoryCode,
        //     "transactionNumber": filterData[0].transactionNumber,
        //     "monthlyAvg": filterData[0].monthlyAvg,
        //     "total": filterData[0].total
        // };
        // const multiRec = await convertMultiRecV1ToV2(filterData, summary);
        // const results = { ...summary, ...multiRec };
        res.status(200).send(jsonData);
    } catch (error) {
        res.status(500).send({ status: false, message: error });
    }
}


const update = async (req, res) => {
    let transDatas = req.body.transactions;
    var bodyArray = [];
    var records = transDatas.records;
    var i;
    for (i = 0; i < records.length; i++) {
        var element = records[i].subRecords;
        var j;
        for (j = 0; j < element.length; j++) {
            var subRecords = element[j];
                if (subRecords.categoryCode) {
                bodyArray.push(subRecords);
                element.splice(j,1)
            }
        }
    }
    if (bodyArray.length > 0) {
        var k;
        for (k = 0; k < bodyArray.length; k++) {
            var l;
            for (l = 0; l < records.length; l++) {
                var element = records[l].subRecords;
                var m;
                for (m = 0; m < element.length; m++) {
                    var subRecords = element[m];
                        if (subRecords.category === bodyArray[k].categoryCode) {
                            bodyArray[k].category = bodyArray[k].categoryCode;
                            delete bodyArray[k].categoryCode;
                            element.push(bodyArray[k]);
                    }
                }
            }
        }
    }
    var n;
    for (n = 0; n < records.length; n++) {
        var element = records[n];
        var total = 0;
        var transData = element.subRecords;
        var o;
        for (o = 0; o < transData.length; o++) {
            var elements = transData[o];
            var credits = elements.credit.replace(",", "");
            total += parseFloat(credits);
        }
        element.headers.total = total.toFixed(2);
        var monthlyAvg = total / 12;
        element.headers.transactionNumber = transData.length;
        element.headers.monthlyAvg = monthlyAvg.toFixed(2);
        element.total.total = total;
    }
    res.status(200).send(req.body);
    // try {
    //     const oldCat = req.body.oldCategoryCode;
    //     let newCat = req.body.newCategoryCode;
    //     let transId = req.body.transactionId;
    //     let olData = '';
    //     let transObj = {};
    //     let filterData = olData.filter(async function (res) {
    //         if (res.categoryCode === req.body.oldCategoryCode) {
    //             const index = res.transactions.findIndex(function (o) {
    //                 if (o.transactionId === req.body.transactionId) transObj = o; return true;
    //             })
    //             if (index !== -1) {
    //                 res.transactions.splice(index, 1);
    //                 const rrr = await calculateTransactions(res);
    //                 return rrr;
    //             }
    //         }
    //         return res;
    //     });
    //     let finalData = filterData.filter(async function (res) {
    //         if (res.categoryCode === req.body.newCategoryCode) {
    //             if (transObj) {
    //                 transObj.category = req.body.newCategoryCode;
    //                 res.transactions.push(transObj);
    //                 const rrr = await calculateTransactions(res);
    //                 return rrr;
    //             }
    //         }

    //         return res;
    //     });
    //     res.status(200).send(finalData);
    // } catch (error) {
    //     res.status(500).send({ status: false, message: error });
    // }
}

const convertMultiRecV1ToV2 = async (multirec) => {
    try {
        const formatMultirec = {
            records: multirec[0].transactions.map(i => {
                return {
                    headers: {
                    },
                    total: {
                    },
                    subRecords: [
                        i
                    ]
                }
            })
        }
        return formatMultirec
    }
    catch (error) {
        throw error;
    }
}
const calculateTransactions = async (data) => {
    let finalData = data;
    let total = 0;
    const transData = finalData.transactions;
    for (let i = 0; i < transData.length; i++) {
        const element = transData[i];
        const credits = element.credit.replace(",", "");
        total += parseFloat(credits);
    }
    finalData.total = total.toFixed(2);
    const monthlyAvg = total / transData.length; // or 
    finalData.transactionNumber = transData.length;
    finalData.monthlyAvg = monthlyAvg.toFixed(2);
    return finalData
}

///////// RDP  /////////////////////////////////
// const convertMultiRecV1ToV2 = async (multirec)=> {
//     try {
//         let formatMultirec = {
//             records: [],
//             total: {}
//         }
//         await multirec.map(res => {
//                 let recordArray = [];
//                 res.transactions.forEach(element => {
//                     if(recordArray.length > 0){
//                         recordArray[0].subRecords.push(element);
//                     }else{
//                         let obj = {};
//                         obj.headers = {description: res.description,
//                         categoryCode: res.categoryCode,transactionNumber: res.transactionNumber, monthlyAvg: res.monthlyAvg, total: res.total};
//                         obj.total = {total: res.total};
//                         obj.subRecords = [element];
//                         recordArray.push(obj);
//                     }
//                 });
//                 formatMultirec.records.push(recordArray[0]);
//             });
        
//             console.log(formatMultirec);
//         return formatMultirec;
//     } catch (error) {
//         console.log(error);
//         throw error;
//     }
// }

module.exports = { initiate, update, getCategory };